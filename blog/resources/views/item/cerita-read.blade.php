@extends('laravel.master2')

@section('header-img')
<div class="tm-welcome-container tm-fixed-header tm-fixed-header-2">
</div>
@endsection

@section('content')
<div class="row justify-content-center mt-5">
    <div class="col-lg-12">
        <h1 style="text-align: center;">{{ $cerita->judul }}</h1>
        <p style="text-align: center;">{{ $cerita->genre->nama }}</p>
        <div class="mt-5 mb-5 card shadow">
            <div class="card-body p-3 ml-5">
                {{($cerita->isi)}}
                <br>
                {{-- <p>{{ $cerita->user->nama_pena }}</p> --}}
            </div>
        </div>
        <div class="mt-5 mb-2 card shadow">
            <div class="card-body p-3 ml-2">
                <P>Add Comment</P>
                <form class="form-horizontal">
                    <div class="input-group input-group-sm mb-0">
                        <input class="form-control form-control-sm" placeholder="Comment">
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-success">Send</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="card shadow">
            <div class="card-body p-3 ml-2">
                <div class="post clearfix">
                    <div class="user-block">
                        <span class="username" style="color:gray">
                            Sarah Ross
                        </span>
                        <span class="description"> - Sent you a comment</span>
                    </div>
                    <input type="text" class="form-control mb-2" id="1" disabled>
                    
                </div>
                <hr />
                <div class="post clearfix">
                    <div class="user-block">
                        <span class="username">
                            <a href="#">Sarah Ross</a>
                            <a href="#" class="float-right btn-tool"></a>
                        </span>
                        <span class="description"> - Sent you a comment</span>
                    </div>
                    <input type="text" class="form-control mb-2" id="2" disabled>
                    <div class="input-group-append">
                        <button type="submit" class="btn-warning btn-sm" style="color:white" onclick="Enable()">Edit</button>
                        &nbsp;&nbsp;&nbsp;
                        <button type="submit" class="btn-danger btn-sm">Delete</button>
                    </div>
                </div>
                <hr />
                <div class="post clearfix">
                    <div class="user-block">
                        <span class="username">
                            <a href="#">Sarah Ross</a>
                            <a href="#" class="float-right btn-tool"></a>
                        </span>
                        <span class="description"> - Sent you a comment</span>
                    </div>
                    <p>
                        Lorem ipsum represents a long-held tradition for designers,
                        typographers and the like. Some people hate it and argue for
                        its demise, but others ignore the hate as they create awesome
                        tools to help create filler text for everyone from bacon lovers
                        to Charlie Sheen fans.
                    </p>
                </div>
                <hr />
                <div class="post clearfix">
                    <div class="user-block">
                        <span class="username">
                            <a href="#">Sarah Ross</a>
                            <a href="#" class="float-right btn-tool"></a>
                        </span>
                        <span class="description"> - Sent you a comment</span>
                    </div>
                    <p>
                        Lorem ipsum represents a long-held tradition for designers,
                        typographers and the like. Some people hate it and argue for
                        its demise, but others ignore the hate as they create awesome
                        tools to help create filler text for everyone from bacon lovers
                        to Charlie Sheen fans.
                    </p>
                </div>
                <hr />
                <div class="post clearfix">
                    <div class="user-block">
                        <span class="username">
                            <a href="#">Sarah Ross</a>
                            <a href="#" class="float-right btn-tool"></a>
                        </span>
                        <span class="description"> - Sent you a comment</span>
                    </div>
                    <p>
                        Lorem ipsum represents a long-held tradition for designers,
                        typographers and the like. Some people hate it and argue for
                        its demise, but others ignore the hate as they create awesome
                        tools to help create filler text for everyone from bacon lovers
                        to Charlie Sheen fans.
                    </p>
                </div>
                <hr />
            </div>
        </div>
    </div>
</div>
@endsection

@push('disableEnable')
<script>
    function Enable() {
        document.getElementById("2").disabled = false;
    }

    function myFunction2() {
        document.getElementById("myText").disabled = true;
    }
</script>
@endpush