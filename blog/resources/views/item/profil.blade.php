@extends('laravel.master2')

@section('header-img')
<div class="tm-welcome-container tm-fixed-header tm-fixed-header-5">
</div>
@endsection

@section('content')
<div class="row justify-content-center">
    <div class="col-lg-12">
        <div class="mt-5 nav-wrapper">
            <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
                <li class="nav-item">
                    <a class="nav-link mb-sm-3 mb-md-0 active" data-toggle="tab" href="#my-profil">My Profile</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link mb-sm-3 mb-md-0" data-toggle="tab" href="#edit-profil">Edit Profile</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link mb-sm-3 mb-md-0" data-toggle="tab" href="#my-story">My Story</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link mb-sm-3 mb-md-0" data-toggle="tab" href="#my-comments">My Comments</a>
                </li>
            </ul>
        </div>

        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong>{{ $message }}</strong>
            </div>
        @endif

        <div class="mt-5 card shadow">
            <div class="card-body">
                <div class="tab-content">
                    <div class="tab-pane fade show active text-center mt-1" id="my-profil">
                        <h1><strong>{{Auth::user()->name}}</strong></h1>
                        <div class="h5 font-weight-500">{{Auth::user()->pena}}</div>
                    </div>
                    <div class="tab-pane fade mt-1" id="edit-profil">
                        <form action="/profil/{id}/edit" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="nama">Nama Lengkap</label>
                                <input type="text" class="form-control" name="name" placeholder="Fullname">
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="penname">Nama Pena</label>
                                <input type="text" class="form-control" name="pena" placeholder="Penname">
                                @error('pena')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-primary btn-md">Update</button>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade mt-1" id="my-story">
                        <h3 class="text-center">My Story</h3>
                        <div class="row p-3 ml-5">
                            <div class="card mt-3 mr-3 mb-3" style="width: 30%;">
                                <img class="card-img-top" src="{{asset('/img/5.jpg')}}">
                                <div class="card-body">
                                    <h4 class="card-title">John Doe</h4>
                                    <p class="card-text">Some example text.</p>
                                    <a href="#" class="btn btn-primary stretched-link">Read</a>
                                </div>
                            </div>
                            <div class="card mt-3 mr-3 mb-3" style="width: 30%;">
                                <img class="card-img-top" src="{{asset('/img/5.jpg')}}">
                                <div class="card-body">
                                    <h4 class="card-title">John Doe</h4>
                                    <p class="card-text">Some example text.</p>
                                    <a href="#" class="btn btn-primary stretched-link">Read</a>
                                </div>
                            </div>
                            <div class="card mt-3 mr-3 mb-3" style="width: 30%;">
                                <img class="card-img-top" src="{{asset('/img/5.jpg')}}">
                                <div class="card-body">
                                    <h4 class="card-title">John Doe</h4>
                                    <p class="card-text">Some example text.</p>
                                    <a href="#" class="btn btn-primary stretched-link">Read</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade mt-1" id="my-comments">
                        <div class="post clearfix">
                            <div class="user-block">
                                <span class="username">
                                    <a href="#">Judul Cerita yang di komen</a>
                                    <a href="#" class="float-right btn-tool"></a>
                                </span>
                                <span class="description"> - You comment</span>
                            </div>
                            <p>
                                Lorem ipsum represents a long-held tradition for designers,
                                typographers and the like. Some people hate it and argue for
                                its demise, but others ignore the hate as they create awesome
                                tools to help create filler text for everyone from bacon lovers
                                to Charlie Sheen fans.
                            </p>
                            <div class="input-group-append">
                                <a href="/editKomen" class="btn-warning btn-sm" style="color:white">Edit</a>
                                &nbsp;&nbsp;&nbsp;
                                <button type="submit" class="btn-danger btn-sm">Delete</button>
                            </div>
                        </div>
                        <hr />
                        <div class="post clearfix">
                            <div class="user-block">
                                <span class="username">
                                    <a href="#">Judul Cerita yang di komen</a>
                                    <a href="#" class="float-right btn-tool"></a>
                                </span>
                                <span class="description"> - You comment</span>
                            </div>
                            <p>
                                Lorem ipsum represents a long-held tradition for designers,
                                typographers and the like. Some people hate it and argue for
                                its demise, but others ignore the hate as they create awesome
                                tools to help create filler text for everyone from bacon lovers
                                to Charlie Sheen fans.
                            </p>
                            <div class="input-group-append">
                                <a href="/editKomen" class="btn-warning btn-sm" style="color:white">Edit</a>
                                &nbsp;&nbsp;&nbsp;
                                <button type="submit" class="btn-danger btn-sm">Delete</button>
                            </div>
                        </div>
                        <hr />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection