<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('item.home');
});

Route::get('/cerita', 'CeritaController@index');
Route::get('/cerita/create', 'CeritaController@create');
Route::post('/cerita', 'CeritaController@store');
Route::get('/cerita/edit/{id}', 'CeritaController@edit');
Route::get('/cerita/read/{id}', 'CeritaController@show');
Route::get('/cerita/edit/{id}', 'CeritaController@edit');
Route::put('/cerita/edit/{id}', 'CeritaController@update');
Route::delete('/cerita/{id}', 'CeritaController@destroy');

// Route::get('/read', function () {
//     return view('item.cerita-read');
// });


Route::get('/profil', 'UserController@index');
Route::put('/profil/{id}/edit', 'UserController@update');


Route::get('/editKomen', function () {
    return view('item.editKomen');
});





Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
