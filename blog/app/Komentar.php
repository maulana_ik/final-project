<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Komentar extends Model
{
    protected $table = 'komentar';

    public function user(){
        return $this->belongsTo('App\User', 'id_user');
    }

    public function cerita(){
        return $this->belongsTo('App\Cerita', 'id_cerita');
    }
}
