<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cerita;

class CeritaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index']);
    }

    public function index()
    {
        $cerita = Cerita::all();
        return view('item.explore', compact('cerita'));
    }

    public function create()
    {
        return view('item.cerita-create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required|max:100)',
            'genre' => 'required',
            'cover' => 'required|image',
            'dekripsi' => 'required|max:255',
            'isi' => 'required',
        ]);

        $file = $request->file('cover');
        $extension = $file->extension();
        $file_name = time() . '.' . $extension;
        $file->move(public_path('img/uploads'), $file_name);

        Cerita::create([
            'judul' => $request['judul'],
            'genre' => $request['genre'],
            'cover' => $file_name,
            'dekripsi' => $request['dekripsi'],
            'isi' => $request['isi']
        ]);
        return redirect('/profil')->with('status',"Berhasil Menambahkan Cerita!");
    }

    public function show($id)
    {
        $cerita = Cerita::find($id);
        return view('item.cerita-read', compact('cerita'));
    }

    public function edit($id)
    {
        $cerita = Cerita::where('id_cerita', $id)->first();
        return view('item.cerita-edit', compact('cerita'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required|max:100)',
            'genre' => 'required',
            'cover' => 'required|image',
            'dekripsi' => 'required|max:255',
            'isi' => 'required',
        ]);

        $file = $request->file('cover');
        $extension = $file->extension();
        $file_name = time() . '.' . $extension;
        $file->move(public_path('img/uploads'), $file_name);

        $cerita = Cerita::where('id',$id)->update([
            'judul'=>$request['judul'],
            'genre'=>$request['genre'],
            'cover'=>$file_name,
            'dekripsi'=>$request['dekripsi'],
            'isi'=>$request['isi']
        ]);

        return redirect('/profil')->with('success','Cerita Berhasil Di Update!');
    }
    public function destroy($id)
    {
        // Cerita::find($id)->delete();
        Cerita::destroy($id);
        return redirect('/profil')->with('status',"Cerita Berhasil Di Hapus!");;
    }
}
